# SIDL

SIDL is a small language used to generate boilerplate code for serialization and
unserialization of messages used by protoipc.

## Objects

### Messages

For now SIDL only supports two object types. The first one is the **message** type.
A message is an object with a service, a name, and optionaly data fields (which
are the message contents).

```cpp
message Broker::CreateProcess {
    string process_name;
}
```

The previous message is for the Broker service (a service is like a namespace),
its name is CreateProcess and it contains a single string parameter. From this
code the SIDL compiler will generate a class inheriting ipc::Message implementing
getters/setters and serialization routines to convert to and from ipc::Message.
It will also generate service specific unique message ids for each message under
the service namespace. The CreateProcess message id can be accessed as follows:

```cpp
messages::broker::CreateProcessId;
```

Note: The service name in a message will be  put in lower case by the compiler.

### Structs

A **struct** is a simple agglomeration of other datatypes.

```cpp
struct block
{
    vec<u8> bytes;
    usize seq_id;
}
```

## Datatypes

The compiler uses the following mappings to convert SIDL types into C++ types.


| SIDL Type   | C++ Type                    |
| ----------- | ----------                  |
| vec         | std::vector                 |
| handle      | int                         |
| optional    | std::optional               |
| string      | std::string                 |
| buffer      | std::vector\<std::uint8_t\> |
| u8          | std::uint8_t                |
| u16         | std::uint16_t               |
| u32         | std::uint32_t               |
| u64         | std::uint64_t               |
| i8          | std::int8_t                 |
| i16         | std::int16_t                |
| i32         | std::int32_t                |
| i64         | std::int64_t                |
| usize       | std::size_t                 |
| service_id  | ipc::ServiceId              |

SIDL supports generic arguments for some types (vec and optional for now), but
only the fields can make use of it (a structure cannot be generic like templated
cpp classes).

Additionally the **handle** type is specific. For now it cannot be used inside
containers (which means no `vec<handle>` and no `optional<handle>`).

The handle type represented as an int as even on windows the libc provides an
emulation of file descriptors over HANDLEs.

## Additional features

The **namespace** specifier can be used to start a namespace.

```cpp
namespace test;

message MyService::MyMessage {
}
```

Normally SIDL generates all messages in a top level **messages::** namespace. With
the namespace specifier the previous code would generate the MyMessage class
inside the **test::messages** namespace. There can be any number of namespace
specifiers, the rule is that an object takes as its namespace the one that was
defined last.

```cpp
namespace ns_a;

message Svc::A # Namespace is ns_a
{}

namespace ns_b;

message Svc::B # Namespace is ns_b
{}

message C # Namespace is ns_b
{}
```

The **typedef** specifier can be used to define a type alias. The only rule is
that the type alias must not be generic.

```cpp
typedef newtype vec<u8>; # works
typedef A<T> u32; # does not work
```

## Code generation

By default the compiler will generate in the current folder a **.hh** header and a
**.cpp** with the following convention `<file.sidl>.<extension>`. You can also specify
an output directory using the **-o/--outdir** option.

By default all classes will be generated under a global **::message** namespace, but
you can specify a top level namespace using the **namespace** statement if needed
(for now it is not possible to create nested namespaces).

For each field in an object SIDL will generate getters and setters with as names
`get_<field name>` and `set_<field name>`. Additionally if more speed is needed the
compiler also generates move setters `set_mov_<field name>`.

## Usage with meson

This project can be used as a meson subproject. It exposes a **sidlcc** generator
object that can be used to generate the c++ code easily. Examples for this can
be found in the `examples/` subdirectory of proto-ipc.

## Style guide

For clarity services names in messages must be identical to the cpp services
using the messages. For example a Broker class inheriting ipc::Service should
have its messages starting with **Broker::**.

Message names in pascal case and fields in snake case are encouraged, but in
the end it is down to personal preference.

The same base name must be used for request/reply messages (messages sent using
**ipc::Service::send_request**) and Request/Reply must be appended at the end
(ex: **Broker::CreateProcessRequest**/**Broker::CreateProcessReply**). For one way
messages any convenient name can be used.

Error handling using an optional field and an error struct is the recommended way
of returning error across processes:

```cpp
struct CreateProcessError {
    string error_message;
    u32 error_code;
}

message Broker::CreateProcessRequest {
    string name;
}

message Broker::CreateProcessReply {
    optional<CreateProcessError> error;
    service_id process_bid;
}
```
