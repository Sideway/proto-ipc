if exists('b:current_syntax')
	finish
endif

let b:current_syntax='sidl'

syn match   sidlComment '#.*$'
syn keyword sidlTop message namespace struct typedef
syn keyword sidlType string service_id

hi link sidlTop Structure
hi link sidlType Type
hi link sidlComment Comment
