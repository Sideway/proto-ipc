from enum import Enum


class TokenType(Enum):
    Namespace = 1
    Message = 2
    Type = 3
    Symbol = 4
    Semicol = 5
    LBrack = 6
    RBrack = 7
    ClassSeparator = 8
    Struct = 9
    Typedef = 10
    GenericLeft = 11
    GenericRight = 12
    Comma = 13
    Optional = 14
    Eof = 15


class Token:
    type: TokenType
    value: str
    line: int
    col: int

    def __init__(self, tktype: TokenType, tkval: str, line: int, col: int):
        self.type = tktype
        self.value = tkval
        self.line = line
        self.col = col

    def __repr__(self) -> str:
        return f"Token(type={self.type}, value='{self.value}', line={self.line}, col={self.col})"

    def __str__(self) -> str:
        return self.value
