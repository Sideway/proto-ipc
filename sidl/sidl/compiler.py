from typing import List, Dict, Optional, Tuple
from pathlib import Path
from sidl.parser import Parser
from sidl.objects import Message, Struct, Typedef, Type, SidlObject, Field


class CompilerOptions:
    input_file: str
    outdir: Optional[str]
    generate_hh: bool
    generate_cpp: bool

    def __init__(self, input_file: str, outdir: Optional[str] = None,
                 generate_hh: bool = True,
                 generate_cpp: bool = True):
        self.input_file = input_file
        self.outdir = outdir
        self.generate_hh = generate_hh
        self.generate_cpp = generate_cpp


class Compiler:
    elements: List[SidlObject]
    outdir: Optional[str]
    options: CompilerOptions
    cpp_template: str
    cpp_wrap_template: str
    hh_template: str
    hh_wrap_template: str
    hh_struct_template: str
    getset_template: str
    service_ids: Dict[str, int]
    types: Dict[str, str]

    def __init__(self, options: CompilerOptions):
        self.options = options
        p = Parser.from_file(self.options.input_file)

        self.elements = p.parse()
        self.outdir = self.options.outdir

        # Loading the cpp/h templates
        data_path = str(Path(__file__).parent) + "/data"
        self.cpp_template = open(f"{data_path}/impl_template.cpp", "r").read()
        self.cpp_wrap_template = open(f"{data_path}/impl_template_wrap.cpp", "r").read()
        self.hh_template = open(f"{data_path}/decl_template.hh", "r").read()
        self.hh_wrap_template = open(f"{data_path}/decl_template_wrap.hh", "r").read()
        self.hh_struct_template = open(f"{data_path}/decl_struct_template.hh", "r").read()
        self.getset_template = open(f"{data_path}/getter_setter.hh").read()

        # Keeps count of the ids used by messages within a service so they are
        # all unique.
        self.service_ids = {}

        self.types = {
                'vec': 'std::vector',
                'optional': 'std::optional',
                'string': 'std::string',
                'buffer': 'std::vector<std::uint8_t>',  # Should it be a builtin ?
                'u8': 'std::uint8_t',
                'u16': 'std::uint8_t',
                'u32': 'std::uint32_t',
                'u64': 'std::uint64_t',
                'i8': 'std::int8_t',
                'i16': 'std::int16_t',
                'i32': 'std::int32_t',
                'i64': 'std::int64_t',
                'usize': 'std::size_t',
                'service_id': 'rpc::ServiceId',
                'handle': 'handle'  # In vlc even windows fd are int.
        }

    def compile(self) -> None:
        if not self.outdir:
            outdir = ""
        else:
            outdir = self.outdir + "/"

        cpp_code = ""
        hh_code = ""

        for element in self.elements:
            if isinstance(element, Message):
                if element.service not in self.service_ids:
                    self.service_ids[element.service] = 1
                else:
                    self.service_ids[element.service] += 1

                cpp_code += self._compile_message_cpp(element)
                hh_code += self._compile_message_h(element)
            elif isinstance(element, Struct):
                self.types[element.name] = element.name
                cpp_code += self._compile_struct_cpp(element)
                hh_code += self._compile_struct_h(element)
            elif isinstance(element, Typedef):
                self.types[element.name] = self._resolve_type(element.type)
            else:
                raise Exception(f"Unknown element type: {element}")

        input_idl_name = Path(self.options.input_file).name

        output_cc_path = outdir + input_idl_name + ".cpp"
        output_hh_path = outdir + input_idl_name + ".hh"

        hh_basename = Path(output_hh_path).name
        include_guard = hh_basename.replace('.', '_').upper()

        cc_edits = {
                "%%message_list%%": cpp_code,
                "%%include_header%%": hh_basename
        }

        hh_edits = {
                "%%message_list%%": hh_code,
                "%%include_guard%%": include_guard
        }

        output_cc = self._replace(self.cpp_wrap_template, cc_edits)
        output_hh = self._replace(self.hh_wrap_template, hh_edits)

        if self.options.generate_cpp:
            open(output_cc_path, "w").write(output_cc)

        if self.options.generate_hh:
            open(output_hh_path, "w").write(output_hh)

    def _compile_message_cpp(self, message: Message) -> str:
        parse_lines, serialize_lines = self._compile_fields_impl(message.fields)
        edits = {
                "%%message_name%%": message.name,
                "%%service_ns%%": message.service.lower(),
                "%%namespace_start%%": "",
                "%%namespace_end%%": "",
                "%%parse_message%%": parse_lines,
                "%%serialize_message%%": serialize_lines
        }

        if message.namespace:
            edits["%%namespace_start%%"] = f"namespace {message.namespace}\n{{"
            edits["%%namespace_end%%"] = "}"

        return self._replace(self.cpp_template, edits)

    def _compile_message_h(self, message: Message) -> str:
        getset = self._compile_getter_setter_(message.fields)
        pvars = self._compile_variables(message.fields)

        edits = {
                "%%message_name%%": message.name,
                "%%message_id%%": str(self.service_ids[message.service]),
                "%%service_ns%%": message.service.lower(),
                "%%namespace_start%%": "",
                "%%namespace_end%%": "",
                "%%getter_setter%%": getset,
                "%%message_variables%%": pvars
        }

        if message.namespace:
            edits["%%namespace_start%%"] = f"namespace {message.namespace}\n{{"
            edits["%%namespace_end%%"] = "}"

        return self._replace(self.hh_template, edits)

    def _compile_struct_cpp(self, struct: Struct) -> str:
        return ""

    def _compile_struct_h(self, struct: Struct) -> str:
        getset = self._compile_getter_setter_(struct.fields)
        vars = self._compile_variables(struct.fields)
        parse_lines, serialize_lines = self._compile_fields_serialize_struct(struct.fields, indent=8)

        edits = {
                "%%struct_name%%": struct.name,
                "%%getter_setter%%": getset,
                "%%struct_variables%%": vars,
                "%%serialize_struct%%": serialize_lines,
                "%%parse_struct%%": parse_lines,
                "%%namespace_start%%": "",
                "%%namespace_end%%": ""
        }

        if struct.namespace:
            edits["%%namespace_start%%"] = f"namespace {struct.namespace}\n{{"
            edits["%%namespace_end%%"] = "}"
            edits["%%struct_name_ns%%"] = f"{struct.namespace}::messages::{struct.name}"
        else:
            edits["%%struct_name_ns%%"] = f"messages::{struct.name}"

        return self._replace(self.hh_struct_template, edits)

    def _compile_fields_impl(self, fields: List[Field], indent: int = 4) -> Tuple[str, str]:
        parse_lines = []
        serialize_lines = []

        handles_lines = []

        for ftype, fname in fields:
            line = " "*indent

            if ftype.name == "handle":
                handles_lines.append(line + "if (handle_idx >= received_handles_.size())")
                handles_lines.append(line + "    throw std::runtime_error(\"Not enough handles received\");")
                handles_lines.append(line + f"SIDLVAR_{fname}_ = received_handles_[handle_idx++];")

                serialize_lines.append(line + f"add_handle(SIDLVAR_{fname}_);")
            else:
                resolved_type = self._resolve_type(ftype)

                read_fn = f"read<{resolved_type}>"
                write_fn = f"write<{resolved_type}>"

                parse_lines.append(line + f"SIDLVAR_{fname}_ = reader.{read_fn}();")
                serialize_lines.append(line + f"writer.{write_fn}(SIDLVAR_{fname}_);")

        # Special case when a message contains handles
        if handles_lines:
            parse_lines.append(line + "std::size_t handle_idx = 0;")
            parse_lines += handles_lines

        return "\n".join(parse_lines), "\n".join(serialize_lines)

    # TODO: Find a cleaner way of doing this
    def _compile_fields_serialize_struct(self, fields: List[Field], indent: int = 4) -> Tuple[str, str]:
        parse_lines = []
        serialize_lines = []

        for ftype, fname in fields:
            line = " "*indent
            resolved_type = self._resolve_type(ftype)

            read_fn = f"read<{resolved_type}>"
            write_fn = f"write<{resolved_type}>"

            parse_lines.append(line + f"obj.SIDLVAR_{fname}_ = reader.{read_fn}();")
            serialize_lines.append(line + f"writer.{write_fn}(obj.SIDLVAR_{fname}_);")

        return "\n".join(parse_lines), "\n".join(serialize_lines)

    def _compile_getter_setter_(self, fields: List[Field], indent: int = 8) -> str:
        getset = ""

        for ftype, fname in fields:
            edits = {
                    "%%field_type%%": self._resolve_type(ftype),
                    "%%field_name%%": fname
            }

            getset += self._replace(self.getset_template, edits)

        return "".join(map(lambda a: " "*indent + a.rstrip() + "\n", getset.split("\n")))

    def _compile_variables(self, fields: List[Field], indent: int = 8) -> str:
        vars: str = ""

        for ftype, fname in fields:
            vars += " "*indent + f"{self._resolve_type(ftype)} SIDLVAR_{fname}_;\n"

        return vars

    def _type_check(self, ty: Type, depth: int = 0) -> None:
        if ty.name not in self.types:
            raise Exception(f"No equivalent c++ type for {ty.name}")

        real_name = self.types[ty.name]

        if real_name == "handle" and depth > 0:
            raise Exception("handle cannot be inside a container")
        elif ty.name == "vec" and (not ty.generics or len(ty.generics) > 1):
            raise Exception(f"vec only accepts exactly 1 type")
        elif ty.name == "optional" and (not ty.generics or len(ty.generics) > 1):
            raise Exception(f"optional only accepts exactly 1 type")

        if ty.generics:
            for sub_ty in ty.generics:
                self._type_check(sub_ty, depth + 1)

    def _resolve_type(self, ty: Type) -> str:
        def resolve_type_sub(type: Type) -> Type:
            result_ty = Type(self.types[type.name])

            if result_ty.name == "handle":
                result_ty.name = "int"

            if not type.templated:
                return result_ty

            result_ty.generics = []

            if type.generics:
                for gen_type in type.generics:
                    result_ty.generics.append(resolve_type_sub(gen_type))

            return result_ty

        self._type_check(ty)
        return str(resolve_type_sub(ty))

    def _replace(self, s: str, d: Dict[str, str]) -> str:
        for k, v in d.items():
            s = s.replace(k, v)

        return s
