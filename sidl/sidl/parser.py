from typing import List, Optional

from sidl.lexer import Lexer
from sidl.token import TokenType, Token
from sidl.objects import Message, Type, Typedef, Struct, SidlObject, Field


class InternalParsingException(Exception):
    msg: str
    line: int
    col: int

    def __init__(self, msg: str, line: int, col: int):
        self.msg = msg
        self.line = line
        self.col = col

    def __str__(self) -> str:
        return f"Parsing error at {self.line}:{self.col} : {self.msg}"


class ParsingException(Exception):
    msg: str
    line: str
    line_num: int
    col_num: int

    def __init__(self, msg: str, line: str, line_num: int, col_num: int):
        self.msg = msg
        self.line = line
        self.line_num = line_num
        self.col_num = col_num

    def __str__(self) -> str:
        result = []
        result.append(f"Parsing error {self.line_num}:{self.col_num}: {self.msg}")
        result.append(self.line)
        result.append(" "*(self.col_num - 1) + "^")

        return "\n".join(result)


class Parser:
    lines: List[str]
    namespace: Optional[str]
    lexer: Lexer

    def __init__(self, data: str):
        self.lines = data.split("\n")
        self.lexer = Lexer(data)
        self.namespace = None

    @staticmethod
    def from_file(path: str) -> 'Parser':
        data = open(path, "r").read()
        return Parser(data)

    def _expect(self, tktype: TokenType) -> Token:
        line = self.lexer.line
        col = self.lexer.col
        tok = self.lexer.next_token()

        if tok is None:
            raise InternalParsingException("Unexpected eof", line, col)

        if tok.type != tktype:
            raise InternalParsingException(f"Expected token {tktype} but got {tok.type}", line, col)

        return tok

    def parse(self) -> List[SidlObject]:
        try:
            return self._parse()
        except InternalParsingException as exc:
            line = self.lines[exc.line - 1]
            raise ParsingException(exc.msg, line, exc.line, exc.col)

    def _parse(self) -> List[SidlObject]:
        result: List[SidlObject] = []

        while (tok := self.lexer.next_token()):
            if tok.type == TokenType.Eof:
                break
            elif tok.type == TokenType.Namespace:
                self._parse_namespace()
            elif tok.type == TokenType.Message:
                result.append(self._parse_message())
            elif tok.type == TokenType.Typedef:
                result.append(self._parse_typedef())
            elif tok.type == TokenType.Struct:
                result.append(self._parse_struct())
            else:
                raise InternalParsingException(f"Unexpected token '{tok.type}'", tok.line, tok.col)

        return result

    def _parse_type(self) -> Type:
        def __parse_type_sub() -> Type:
            typename = self._expect(TokenType.Symbol)
            generics = None
            first = self.lexer.peek_token()

            if first.type == TokenType.Eof:
                raise InternalParsingException("Unexpected eof", self.lexer.line,
                                               self.lexer.col)

            # detect '<'
            if first.type == TokenType.GenericLeft:
                self.lexer.next_token()
                generics = []
                expected_type = False

                while True:
                    tok = self.lexer.peek_token()

                    if tok.type == TokenType.GenericRight:
                        # We had a comma but no type was provided: A<B,>
                        if expected_type:
                            raise InternalParsingException(f"Expected type but got nothing",
                                                           tok.line, tok.col)

                        self.lexer.next_token()
                        break

                    expected_type = False
                    generics.append(__parse_type_sub())

                    tok = self.lexer.peek_token()

                    if tok.type == TokenType.GenericRight:
                        self.lexer.next_token()
                        break

                    if tok.type != TokenType.Comma:
                        raise InternalParsingException(f"{TokenType.Comma} expected but got {tok.type}",
                                                       tok.line, tok.col)

                    expected_type = True
                    self.lexer.next_token()

                if len(generics) == 0:
                    raise InternalParsingException(f"Templated type '{typename.value}' must have at least 1 generic argument",
                                           typename.line, typename.col)

                return Type(typename.value, generics=generics)
            else:
                return Type(typename.value)

        return __parse_type_sub()

    def _parse_typedef(self) -> Typedef:
        alias_name = self._expect(TokenType.Symbol)
        alias_type = self._parse_type()
        self._expect(TokenType.Semicol)

        return Typedef(alias_name.value, alias_type)

    def _parse_field(self) -> Field:
        field_type = self._parse_type()
        field_name = self._expect(TokenType.Symbol)

        return Field(field_type, field_name.value)

    def _parse_struct(self) -> Struct:
        struct_name = self._expect(TokenType.Symbol)
        self._expect(TokenType.LBrack)

        result: Struct = Struct(struct_name.value, self.namespace)

        while True:
            tok = self.lexer.peek_token()
            last_line = tok.line
            last_col = tok.col

            if tok.type == TokenType.RBrack:
                self.lexer.next_token()
                break
            elif tok.type == TokenType.Eof:
                raise InternalParsingException("Missing closing '}' after struct", last_line, last_col)

            result.fields.append(self._parse_field())
            self._expect(TokenType.Semicol)

        return result

    def _parse_namespace(self) -> None:
        ns_tok = self._expect(TokenType.Symbol)
        _ = self._expect(TokenType.Semicol)
        self.namespace = ns_tok.value

    def _parse_message(self) -> Message:
        service_name = self._expect(TokenType.Symbol)
        self._expect(TokenType.ClassSeparator)
        self._expect(TokenType.ClassSeparator)
        message_name = self._expect(TokenType.Symbol)

        # Now parse all the fields
        self._expect(TokenType.LBrack)

        result = Message(service_name.value, message_name.value, self.namespace)

        while True:
            tok = self.lexer.peek_token()
            last_line = tok.line
            last_col = tok.col

            if tok.type == TokenType.RBrack:
                self.lexer.next_token()
                break
            elif tok.type == TokenType.Eof:
                raise InternalParsingException("Missing closing '}' after struct", last_line, last_col)

            result.fields.append(self._parse_field())
            self._expect(TokenType.Semicol)

        if not tok:
            raise InternalParsingException("Missing closing '}' after message", last_line, last_col)

        return result
