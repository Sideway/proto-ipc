from typing import List, NamedTuple, Optional
from collections import namedtuple


class Type:
    name: str
    generics: Optional[List['Type']]

    def __init__(self, name: str, generics: Optional[List['Type']] = None):
        self.name = name
        self.generics = generics

    @property
    def templated(self) -> bool:
        return self.generics is not None

    def __str__(self) -> str:
        if self.generics is not None:
            gen = ", ".join(str(s) for s in self.generics)
            return f"{self.name}<{gen}>"

        return f"{self.name}"

    def __repr__(self) -> str:
        if self.generics is not None:
            gen = ", ".join(repr(t) for t in self.generics)
            return f"Type(name={self.name}, generics = [{gen}])"

        return f"Type(name={self.name})"


class SidlObject:
    pass


class Field(NamedTuple):
    type: Type
    name: str


class Message(SidlObject):
    service: str
    name: str
    namespace: Optional[str]
    fields: List[Field]

    def __init__(self, service: str, name: str, namespace: Optional[str]):
        self.service = service
        self.name = name
        self.namespace = namespace
        self.fields = []

    def add_field(self, type: Type, name: str) -> None:
        self.fields.append(Field(type, name))

    def __repr__(self) -> str:
        return f"Message(service={self.service}, name={self.name}, namespace={self.namespace}, fields={self.fields}"


class Typedef(SidlObject):
    name: str
    type: Type

    def __init__(self, name: str, type: Type):
        self.name = name
        self.type = type

    def __repr__(self) -> str:
        return f"Typedef(typename={self.name}, type={repr(self.type)})"


class Struct(SidlObject):
    name: str
    namespace: Optional[str]
    fields: List[Field]

    def __init__(self, name: str, namespace: Optional[str]):
        self.name = name
        self.namespace = namespace
        self.fields = []

    def add_field(self, type: Type, name: str) -> None:
        self.fields.append(Field(type, name))

    def __repr__(self) -> str:
        return f"Struct(name={self.name}, namespace={self.namespace}, fields={self.fields})"
