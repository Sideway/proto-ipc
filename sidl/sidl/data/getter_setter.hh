void set_%%field_name%%(%%field_type%% value)
{
    SIDLVAR_%%field_name%%_ = value;
}

void set_move_%%field_name%%(%%field_type%%&& value)
{
    SIDLVAR_%%field_name%%_ = std::move(value);
}

%%field_type%% get_%%field_name%%() const
{
    return SIDLVAR_%%field_name%%_;
}
