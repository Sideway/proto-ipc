%%namespace_start%%

namespace messages
{

namespace %%service_ns%%
{
    static constexpr rpc::MessageType %%message_name%%Id = %%message_id%%;

    class %%message_name%%: public rpc::Message
    {
    public:
        %%message_name%%(rpc::ServiceId source, rpc::ServiceId destination);
        %%message_name%%(rpc::Message&& message);

%%getter_setter%%
    protected:
        virtual void serialize_body_(rpc::BinaryWriter& writer) override;

    private:
%%message_variables%%
    };
}

}

%%namespace_end%%
