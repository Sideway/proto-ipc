%%namespace_start%%

namespace messages
{

namespace %%service_ns%%
{

%%message_name%%::%%message_name%%(rpc::ServiceId source, rpc::ServiceId destination)
    : rpc::Message(source, destination, %%message_name%%Id)
{}

%%message_name%%::%%message_name%%(rpc::Message&& message)
    : rpc::Message(std::move(message))
{
    if (type_ != %%message_name%%Id)
        throw std::runtime_error("Invalid message type");

    rpc::BinaryReader reader(std::move(data_));

%%parse_message%%
}

void %%message_name%%::serialize_body_(rpc::BinaryWriter& writer)
{
%%serialize_message%%
}

}

}

%%namespace_end%%
