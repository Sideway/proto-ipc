#ifndef %%include_guard%%
#define %%include_guard%%

#include "protorpc/message.hh"
#include "protorpc/binarywriter.hh"
#include "protorpc/binaryreader.hh"

%%message_list%%

#endif
