%%namespace_start%%

namespace messages
{
    struct %%struct_name%%
    {
%%struct_variables%%

%%getter_setter%%
    };
}

%%namespace_end%%

template <>
struct rpc::serializable<%%struct_name_ns%%>
{
    void serialize(%%struct_name_ns%%& obj, rpc::BinaryWriter& writer)
    {
%%serialize_struct%%
    }

    void unserialize(%%struct_name_ns%%& obj, rpc::BinaryReader& reader)
    {
%%parse_struct%%
    }
};
