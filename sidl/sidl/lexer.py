from typing import Optional
from sidl.token import Token, TokenType


class Lexer:
    col: int
    line: int
    pos: int
    data: str
    peek: Optional[Token]

    def __init__(self, data: str):
        self.col = 1
        self.line = 1
        self.pos = 0
        self.data = data
        self.peek = None

    def next_char(self) -> Optional[str]:
        if self.pos >= len(self.data):
            return None

        c = self.data[self.pos]
        self.pos += 1

        if c == '\n':
            self.col = 1
            self.line += 1
        else:
            self.col += 1

        return c

    def peek_char(self) -> Optional[str]:
        if self.pos >= len(self.data):
            return None

        return self.data[self.pos]

    def peek_token(self) -> Token:
        if not self.peek:
            self.peek = self.next_token()
            return self.peek
        else:
            return self.peek

    def next_token(self) -> Token:
        if self.peek:
            result = self.peek
            self.peek = None

            return result

        self._skip_ws()
        self.peek = None
        tkval: Optional[str] = None
        tktype = TokenType.Eof

        separators = {
                '{': TokenType.LBrack,
                '}': TokenType.RBrack,
                ';': TokenType.Semicol,
                ':': TokenType.ClassSeparator,
                '<': TokenType.GenericLeft,
                '>': TokenType.GenericRight,
                ',': TokenType.Comma
        }

        start_line = self.line
        start_col = self.col

        while (c := self.peek_char()):
            if c.isspace():
                break

            if tkval is None:
                tkval = ""
                tktype = TokenType.Symbol

            if c in separators:
                if tkval:
                    break

                tkval = self.next_char()
                tktype = separators[c]
                break

            # Ugly but makes mypy happy
            if v := self.next_char():
                tkval += v

        if tkval is None:
            return Token(TokenType.Eof, "", start_line, start_col)

        if tktype == TokenType.Symbol:
            tktype = self._type_token(tkval)

        return Token(tktype, tkval, start_line, start_col)

    def _skip_comment(self) -> None:
        self.next_char()  # eat '#'

        while (c := self.peek_char()):
            if c is None or c == '\n':
                break

            self.next_char()

    def _skip_ws(self) -> None:
        while (c := self.peek_char()):
            if c == '#':
                self._skip_comment()
                continue

            if c != '\n' and not c.isspace():
                return

            self.next_char()

    def _type_token(self, data: str) -> TokenType:
        reserved_words = {
                'namespace': TokenType.Namespace,
                'message': TokenType.Message,
                'struct': TokenType.Struct,
                'typedef': TokenType.Typedef,
        }

        return reserved_words.get(data, TokenType.Symbol)
