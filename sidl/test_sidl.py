import pytest
from sidl.parser import Parser, ParsingException
from sidl.lexer import Lexer
from sidl.objects import Typedef, Struct, Message
from sidl.token import Token, TokenType


def test_parse_typedef_success():
    typedef = "typedef buffer string;"
    p = Parser(typedef)
    result = p.parse()

    assert len(result) == 1  # Only a typedef object
    assert isinstance(result[0], Typedef)
    assert result[0].name == "buffer"
    assert result[0].type.name == "string"


def test_parse_typedef_fail():
    typedef = "typedef buffer string"
    p = Parser(typedef)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_typedef_templated():
    typedef = "typedef a vec<vec<string>>;"
    p = Parser(typedef)
    result = p.parse()

    assert len(result) == 1

    templated_ty = result[0].type
    assert templated_ty.templated
    assert templated_ty.name == "vec"

    sub_ty = templated_ty.generics[0]
    assert sub_ty.templated
    assert sub_ty.name == "vec"

    sub_sub_ty = sub_ty.generics[0]
    assert sub_sub_ty.name == "string"
    assert not sub_sub_ty.templated


def test_parse_typedef_templated_fail():
    typedef = "typedef a vec<"
    p = Parser(typedef)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_struct_empty():
    struct = "struct a {}"
    p = Parser(struct)
    result = p.parse()

    assert len(result) == 1

    s = result[0]
    assert isinstance(s, Struct)
    assert s.name == "a"
    assert len(s.fields) == 0


def test_parse_struct_empty_fail():
    struct = "struct {}"
    p = Parser(struct)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_struct_not_terminated_1():
    struct = "struct zzz { u32 a;"
    p = Parser(struct)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_struct_not_terminated_2():
    struct = "struct zzz {"
    p = Parser(struct)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_struct_fields():
    struct = "struct t { u32 a; vec<u32> b; string d; }"
    p = Parser(struct)
    result = p.parse()

    assert len(result) == 1

    s = result[0]
    assert s.name == "t"
    assert len(s.fields) == 3

    a_field = s.fields[0]
    b_field = s.fields[1]
    d_field = s.fields[2]

    assert a_field.name == "a"
    assert b_field.name == "b"
    assert d_field.name == "d"
    assert str(a_field.type) == "u32"
    assert str(b_field.type) == "vec<u32>"
    assert str(d_field.type) == "string"


def test_parse_struct_nested_fail():
    struct = "struct t { struct a { u32 b; } c; }"
    p = Parser(struct)

    with pytest.raises(ParsingException):
        p.parse()


def test_templated_multiple():
    test = "struct t { A<B, C, D> v; }"
    p = Parser(test)
    result = p.parse()

    assert len(result) == 1

    ty = result[0].fields[0].type
    assert len(ty.generics) == 3


def test_templated_empty_fail():
    test = "struct t { A<> v; }"
    p = Parser(test)

    with pytest.raises(ParsingException):
        p.parse()


def test_templated_space_fail():
    test = "struct t { A<B C> v; }"
    p = Parser(test)

    with pytest.raises(ParsingException):
        p.parse()


def test_parse_struct_leading_comma_fail():
    test = "struct t { A<B,> v; }"
    p = Parser(test)

    with pytest.raises(ParsingException):
        p.parse()


def test_no_namespace():
    test = "struct ans {}"
    p = Parser(test)
    result = p.parse()

    assert len(result) == 1
    assert result[0].namespace is None


def test_multiple_namespaces():
    test = "namespace a; struct ans {} namespace b; struct bns {}"
    p = Parser(test)
    result = p.parse()

    assert len(result) == 2

    ans = result[0]
    bns = result[1]

    assert ans.namespace == "a"
    assert bns.namespace == "b"


def test_message_empty():
    test = "message a::b {}"
    p = Parser(test)
    result = p.parse()

    assert len(result) == 1

    msg = result[0]
    assert isinstance(msg, Message)
    assert msg.service == "a"
    assert msg.name == "b"
    assert len(msg.fields) == 0


def test_message_simple():
    test = "message a::b { vec<u64> d; vec<vec<map<u32, string>>> e; }"
    p = Parser(test)
    result = p.parse()

    assert len(result) == 1

    msg = result[0]
    assert isinstance(msg, Message)
    assert msg.service == "a"
    assert msg.name == "b"
    assert len(msg.fields) == 2

    assert str(msg.fields[0].type) == "vec<u64>"
    assert str(msg.fields[1].type) == "vec<vec<map<u32, string>>>"
