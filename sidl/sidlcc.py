#!/usr/bin/env python3

import argparse
from sidl.compiler import Compiler, CompilerOptions
from sidl.parser import Parser, ParsingException


def main():
    parser = argparse.ArgumentParser(description="proto-ipc Small IDL compiler")
    parser.add_argument(
        "-o", "--outdir", help="Output directory for the generated files", default="",
    )
    parser.add_argument(
            "--header-only",  help="Only generate the header file", action="store_true"
    )
    parser.add_argument(
        "--source-only", help="Only generate the cpp file", action="store_true"
    )
    parser.add_argument("idl_file", help="Input idl file")

    args = parser.parse_args()
    options = CompilerOptions(args.idl_file, outdir=args.outdir)

    if args.header_only and not args.source_only:
        options.generate_cpp = False
    elif not args.header_only and args.source_only:
        options.generate_hh = False

    try:
        c = Compiler(options)
        c.compile()
    except ParsingException as err:
        print(err)


if __name__ == "__main__":
    main()
