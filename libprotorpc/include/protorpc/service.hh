#ifndef NODE_HH
#define NODE_HH

#include <map>
#include <future>
#include <queue>
#include <mutex>
#include <string>
#include <functional>
#include <random>
#include "protorpc/message.hh"

namespace rpc
{
    /*
     * Object representing a self contained service.
     */
    class Service
    {
        using MessageHook = std::function<void(Service&, Message&)>;

    public:
        Service(ServiceId id);
        Service(const Service& other) = delete;
        Service& operator=(const Service& other) = delete;

        virtual ~Service()
        {};

        /*
         * Reads the next message from any source
         */
        virtual Message next_message() = 0;

        /*
         * Sends a one way message.
         */
        virtual void send_message(Message& message) = 0;

        /*
         * Sends a message expecting an answer.
         */
        virtual std::future<Message> send_request(Message& message) = 0;

        /*
         * Forwards a message if we are not the target. The default is to drop.
         */
        virtual void forward_message(Message& message)
        {};

        /*
         * Function run in parallel from the dispatch and input loop.
         */
        virtual void init()
        {};

        /*
         * Loops indefinitely. Serving requests, invoking callbacks and running
         * user defined code.
         */
        void loop();

        /*
         * Generates a unique token that can be used to wait for an reply.
         */
        MessageToken generate_token();

        /*
         * Registers a request for reply. Returns a future that can be awaited
         * for the reply message.
         */
        std::future<Message> register_reply(Message& message);

        /*
         * Add callback hook on message type.
         * TODO: Find how to add our derived type instead of the base type as
         *       the first callback parameter.
         */
        template <typename F>
        void bind(MessageType type, F hook)
        {
            handlers_[type] = hook;
        }

        ServiceId id() const
        {
            return id_;
        }

    private:
        /*
         * Event loop enqueing messages for the dispatch loop.
         */
        void input_loop_();

        /*
         * Event loop dispatching events gathered by the input loop.
         */
        void dispatch_loop_();

        /*
         * Program function wrapper. Used to catch and forward exceptions.
         */
        void init_();

        /*
         * Exception promise. Used to propagate exceptions from async threads
         * back toward the top level caller.
         */
        std::promise<void> exception_forwarder_;

        /*
         * Promise waiting to be set
         */
        std::map<MessageToken, std::promise<Message>> pending_promises_;

        /*
         * Messages enqueued by the event loop.
         */
        std::queue<Message> messages_;

        /*
         * Mutex for the message queue.
         */
        std::mutex messages_lock_;

        /*
         * Condition variable for the dispatch.
         */
        std::condition_variable queue_cv_;

        /*
         * Map of callbacks
         */
        std::map<MessageType, MessageHook> handlers_;

        /*
         * Prng for generating message tokens.
         */
        std::mt19937_64 token_generator_;

        /*
         * Service id
         */
        ServiceId id_;
    };
}

#endif
