#ifndef MESSAGE_HH
#define MESSAGE_HH

#include <cstdint>
#include <vector>
#include <string>
#include <type_traits>

#include "protorpc/binarywriter.hh"

namespace rpc
{
    using ServiceId = std::uint64_t;
    using MessageToken = std::uint64_t;
    using MessageType = std::uint64_t;

    /*
     * Token used when message doesn't expect a reply.
     */
    constexpr MessageToken NullToken = 0;

    /*
     * By convention a broker has an Id of 0
     */
    constexpr ServiceId BrokerService = 0;

    /*
     * Simple raw bytes container for a message
     */
    class Message
    {
    public:
        Message(ServiceId source, ServiceId destination, MessageType type);
        Message(ServiceId source, ServiceId destination, MessageType type, std::vector<std::uint8_t>&& data);
        Message(std::vector<std::uint8_t>&& data, std::vector<int>&& handles);

        Message(const Message& other);
        Message& operator=(const Message& other);
        Message(Message&& other);
        Message& operator=(Message&& other);

        virtual ~Message() = default;

        Message reply() const;

        MessageToken token() const
        {
            return token_;
        }

        MessageType type() const
        {
            return type_;
        }

        ServiceId destination() const
        {
            return destination_;
        }

        ServiceId source() const
        {
            return source_;
        }

        void set_token(MessageToken token)
        {
            token_ = token;
        }

        std::vector<std::uint8_t> serialize()
        {
            rpc::BinaryWriter writer;
            serialize_header_(writer);
            serialize_body_(writer);

            return writer.data();
        }

        std::vector<std::uint8_t> data()
        {
            rpc::BinaryWriter writer;
            serialize_body_(writer);

            return writer.data();
        }

        void add_handle(int fd)
        {
            received_handles_.push_back(fd);
        }

        bool has_handles() const
        {
            return !received_handles_.empty();
        }

        std::vector<int> handles() const
        {
            return received_handles_;
        }

    protected:
        void serialize_header_(rpc::BinaryWriter& writer)
        {
            writer.write(source_);
            writer.write(destination_);
            writer.write(type_);
            writer.write(token_);
        }

        virtual void serialize_body_(rpc::BinaryWriter& writer)
        {
            writer.write(data_.data(), data_.size());
        }

        ServiceId source_;
        ServiceId destination_;
        MessageType type_ = 0;
        MessageToken token_ = NullToken;

        /*
         * XXX: Ugly. Used when parsing an rpc::Message into a defined message.
         */
        std::vector<std::uint8_t> data_;
        std::vector<int> received_handles_;
    };
}

#endif
