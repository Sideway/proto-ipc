#ifndef BINARYWRITER_HH
#define BINARYWRITER_HH

#include <vector>
#include <string>
#include <optional>
#include <type_traits>
#include <cstdint>

#include "protorpc/serializable.hh"

namespace rpc
{
    class BinaryWriter
    {
    public:
        BinaryWriter() = default;

        template <typename T>
        void write(T value)
        {
            write_into(value);
        }

        template <typename T>
        std::enable_if_t<rpc::is_serializable_v<T>, void>
        write_into(T& obj)
        {
            rpc::serializable<T>().serialize(obj, *this);
        }

        template <typename T>
        void write_into(std::vector<T> vec)
        {
            write<std::size_t>(vec.size());

            for (auto& e : vec)
                write(e);
        }

        template <typename T>
        void write_into(std::optional<T> opt)
        {
            if (!opt)
            {
                write<std::uint8_t>(0);
                return;
            }

            write<std::uint8_t>(1);
            write<T>(*opt);
        }

        template <typename T>
        void write(const T* data, std::size_t size)
        {
            const std::uint8_t* start = reinterpret_cast<const std::uint8_t*>(data);
            const std::uint8_t* end = start + (size * sizeof(T));

            buffer_.insert(buffer_.end(), start, end);
        }

        template <typename T>
        std::enable_if_t<std::is_integral_v<T>, void>
        write_into(T value)
        {
            std::uint8_t* start = reinterpret_cast<std::uint8_t*>(&value);
            buffer_.insert(buffer_.end(), start, start + sizeof(T));
        }

        void write_into(std::string str)
        {
            write<std::size_t>(str.size());
            buffer_.insert(buffer_.end(), str.data(), str.data() + str.size());
        }

        std::vector<std::uint8_t> data()
        {
            return buffer_;
        }

    private:

        std::vector<std::uint8_t> buffer_;
    };
}

#endif
