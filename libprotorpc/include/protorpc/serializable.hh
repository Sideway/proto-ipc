#ifndef SERIALIZABLE_HH
#define SERIALIZABLE_HH

#include <type_traits>

namespace rpc
{
    template <typename T>
    struct serializable
    {
        serializable() = delete;
    };

    /*
     * Objects wanting to be serialized and unserialized must implement the
     * serializable trait.
     *
     * template <>
     * struct serializable<SerializableTy>
     * {
     *     void serialize(SerializableTy& obj, ipc::BinaryWrite& writer)
     *     {
     *         // write object fields to writer
     *     }
     *
     *     void unserialize(SerializableTy& obj, ipc::BinaryReader& reader)
     *     {
     *         // read fields from reader
     *     }
     */

    template <typename T>
    constexpr bool is_serializable_v = std::is_constructible_v<serializable<T>>;
}

#endif
