#ifndef BINARYREADER_HH
#define BINARYREADER_HH

#include <vector>
#include <string>
#include <optional>
#include <stdexcept>
#include <cstdint>
#include <type_traits>

#include "protorpc/serializable.hh"

namespace rpc
{
    /*
     * Helper class to read formatted data from a stream.
     * TODO: Find a way to do proper error handling
     */
    class BinaryReader
    {
    public:
        BinaryReader(std::uint8_t* data, std::size_t size);
        BinaryReader(std::vector<std::uint8_t>&& data);

        template <typename T>
        T read()
        {
            T value;
            read_into(value);

            return value;
        }

        template <typename T>
        std::enable_if_t<std::is_integral_v<T>, void>
        read_into(T& result)
        {
            if (index_ + sizeof(T) > data_.size())
                throw std::runtime_error("Not enough data");

            T* value = reinterpret_cast<T*>(data_.data() + index_);
            index_ += sizeof(T);
            result = *value;
        }

        template <typename T>
        std::enable_if_t<rpc::is_serializable_v<T>, void>
        read_into(T& result)
        {
            rpc::serializable<T>().unserialize(result, *this);
        }

        template <typename T>
        void read_into(std::vector<T>& vec)
        {
            std::size_t count = read<std::size_t>();

            for (std::size_t i = 0; i < count; i++)
                vec.push_back(std::move(read<T>()));
        }

        template <typename T>
        void read_into(std::optional<T>& opt)
        {
            std::uint8_t is_present = read<std::uint8_t>();

            if (!is_present)
            {
                opt = std::nullopt;
                return;
            }

            opt = read<T>();
        }

        void read_into(std::string& result)
        {
            std::size_t size = read<std::size_t>();

            if (index_ + size > data_.size())
                throw std::runtime_error("Not enough data");

            char *start = reinterpret_cast<char*>(data_.data() + index_);
            index_ += size;
            result = std::string(start, size);
        }

        std::vector<uint8_t> read_remaining();

    private:
        std::vector<std::uint8_t> data_;
        std::size_t index_;
    };
}

#endif
