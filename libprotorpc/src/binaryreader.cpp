#include "protorpc/binaryreader.hh"

namespace rpc
{

BinaryReader::BinaryReader(std::uint8_t* data, std::size_t size)
    : data_(data, data + size), index_(0)
{}

BinaryReader::BinaryReader(std::vector<std::uint8_t>&& other)
    : data_(std::move(other)), index_(0)
{}

std::vector<std::uint8_t> BinaryReader::read_remaining()
{
    if (index_ >= data_.size())
        return {};

    std::uint8_t* start = data_.data() + index_;
    std::uint8_t* end = data_.data() + data_.size();

    index_ = data_.size();

    return std::vector<std::uint8_t>(start, end);
}

}
