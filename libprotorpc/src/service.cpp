#include <cstdlib>
#include <ctime>
#include "protorpc/service.hh"

namespace rpc
{

Service::Service(ServiceId id)
    : id_(id)
{
    std::srand(std::time(0));
    token_generator_.seed(std::rand());
}

void Service::dispatch_loop_()
{
    try
    {
        for (;;)
        {
            std::unique_lock<std::mutex> lock(messages_lock_);
            queue_cv_.wait(lock, [&]() { return !messages_.empty(); });

            Message msg = std::move(messages_.front());
            messages_.pop();

            if (msg.destination() != id_)
            {
                forward_message(msg);
                continue;
            }

            // If the message is for us then there must be an handler.
            auto it = handlers_.find(msg.type());

            // TODO: Should we silently drop ?
            if (it != handlers_.end())
                it->second(*this, msg);
        }
    }
    catch (...)
    {
        exception_forwarder_.set_exception(std::current_exception());
    }
}

void Service::input_loop_()
{
    try
    {
        for (;;)
        {
            Message msg = next_message();

            auto it = pending_promises_.find(msg.token());

            // If we had a pending promise for a given request so we fulfill it.
            if (it != pending_promises_.end() && msg.token() != NullToken)
            {
                // Resolve the awaiting request.
                it->second.set_value(std::move(msg));

                // We can delete the promise as the future still holds a pointer to
                // the data.
                pending_promises_.erase(it);
            }
            else
            {
                // Message not related to a request. We simple forward it to the
                // dispatch loop
                std::unique_lock<std::mutex> lock(messages_lock_);
                messages_.push(std::move(msg));
                queue_cv_.notify_one();
            }
        }
    }
    catch (...)
    {
        exception_forwarder_.set_exception(std::current_exception());
    }
}

void Service::init_()
{
    try
    {
        init();
    }
    catch (...)
    {
        exception_forwarder_.set_exception(std::current_exception());
    }
}

void Service::loop()
{
    std::thread input(&Service::input_loop_, this);
    std::thread dispatch(&Service::dispatch_loop_, this);

    input.detach();
    dispatch.detach();

    // Running user defined constructor
    init_();

    // Waiting for an exception
    auto future = exception_forwarder_.get_future();

    future.get();
}

std::future<Message> Service::register_reply(Message& message)
{
    message.set_token(generate_token());

    std::promise<Message> future_reply;
    std::future<Message> awaitable = future_reply.get_future();
    pending_promises_.emplace(message.token(), std::move(future_reply));

    return awaitable;
}

MessageToken Service::generate_token()
{
    MessageToken token = token_generator_();

    for (;;)
    {
        auto it = pending_promises_.find(token);

        if (it == pending_promises_.end())
            break;

        token = token_generator_();
    }

    return token;
}

}
