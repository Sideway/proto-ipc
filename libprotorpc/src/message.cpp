#include <fmt/core.h>
#include <cstring>
#include "protorpc/message.hh"
#include "protorpc/binaryreader.hh"

namespace rpc
{

Message::Message(ServiceId source, ServiceId id, MessageType type)
    : source_(source), destination_(id), type_(type)
{}

Message::Message(ServiceId source, ServiceId destination, MessageType type, std::vector<std::uint8_t>&& data)
    : source_(source), destination_(destination), type_(type), data_(std::move(data))
{}

Message::Message(std::vector<std::uint8_t>&& data, std::vector<int>&& handles)
{
    received_handles_ = std::move(handles);

    rpc::BinaryReader reader(std::move(data));
    source_ = reader.read<rpc::ServiceId>();
    destination_ = reader.read<rpc::ServiceId>();
    type_ = reader.read<rpc::MessageType>();
    token_ = reader.read<rpc::MessageToken>();
    data_ = reader.read_remaining();
}

Message& Message::operator=(const Message& other)
{
    source_ = other.source_;
    destination_ = other.destination_;
    token_ = other.token_;
    type_ = other.type_;
    data_ = other.data_;
    received_handles_ = other.received_handles_;

    return *this;
}

Message::Message(const Message& other)
{
    *this = other;
}

Message& Message::operator=(Message&& other)
{

    if (this == &other)
        return *this;

    source_ = other.source_;
    destination_ = other.destination_;
    token_ = other.token_;
    type_ = other.type_;
    data_ = std::move(other.data_);
    received_handles_ = std::move(other.received_handles_);

    return *this;
}

Message::Message(Message&& other)
{
    *this = std::move(other);
}

Message Message::reply() const
{
    Message m(destination_, source_, type_);
    m.set_token(token_);

    return m;
}

}
