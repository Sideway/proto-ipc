#include "protorpc/message.hh"
#include "gtest/gtest.h"

TEST(rpc_test, serialize_simple_round_trip)
{
    std::vector<std::uint8_t> source_data = {
        0x85, 0xde, 0x59, 0x8e, 0xe9, 0xf4, 0xee, 0x80, 0xc4, 0x35, 0xbb, 0xc0,
        0xd2, 0xb4, 0xd5, 0x9, 0x58, 0x74, 0xb, 0xe, 0xa1, 0x57, 0x65, 0x8d,
        0x33, 0xf4, 0x13, 0x32, 0x68, 0xc6
    };

    rpc::Message A(0x4141, 0x4242, 0x4343, std::move(source_data));
    A.set_token(0xdeadbeefdeadbeef);

    std::vector<std::uint8_t> payload = A.serialize();
    std::vector<int> handles = A.handles();

    ASSERT_EQ(handles.size(), 0);

    rpc::Message B(std::move(payload), std::move(handles));

    ASSERT_EQ(B.source(), A.source());
    ASSERT_EQ(B.destination(), A.destination());
    ASSERT_EQ(B.type(), A.type());
    ASSERT_EQ(B.token(), A.token());
    ASSERT_EQ(B.data(), A.data());
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
