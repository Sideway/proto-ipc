#include <string>
#include <map>
#include <cstdlib>
#include <stdexcept>
#include <type_traits>
#include <fmt/core.h>

#include "broker.hh"
#include "access.hh"
#include "transform.hh"
#include "output.hh"

#include "simple_pipeline.sidl.hh"

template <typename T, typename = std::enable_if_t<std::is_base_of_v<pipeline::ClientService, T>>>
void launch_service(rpc::ServiceId id, ipc::Port port)
{
    T service(id, port);

    try
    {
        service.loop();
    }
    catch (std::exception& e)
    {
        pipeline::messages::broker::ReportError req(id, rpc::BrokerService);
        req.set_error_message(e.what());
        // rpc::Message msg(id, rpc::BrokerService, pipeline::Broker::ReportError);
        // msg.append(std::string(e.what()));

        service.send_message(req);
    }
}

ipc::Port string_to_port(std::string port_str)
{
#ifdef __linux__
    return ipc::Port(atoi(port_str.c_str()));
#else
    #error "Unsupported platform"
#endif
}

int main(int argc, char** argv)
{
    std::map<std::string, std::string> options;

    for (int i = 1; i < (argc - 1); i += 2)
    {
        std::string key = argv[i];
        std::string value = argv[i + 1];
        options[key] = value;
    }

    auto type = options.find("--kind");

    if (type == options.end())
    {
        fmt::print("Usage: {} --kind <process kind> [options]\n", argv[0]);
        return 1;
    }

    if (type->second == "broker")
        pipeline::Broker().loop();

    rpc::ServiceId id = atoi(options["--bid"].c_str());
    ipc::Port port = string_to_port(options["--port"]);

    if (type->second == "access")
        launch_service<pipeline::Access>(id, port);
    else if (type->second == "transform")
        launch_service<pipeline::Transform>(id, port);
    else if (type->second == "output")
        launch_service<pipeline::Output>(id, port);
    else
        fmt::print("Invalid process kind: {}\n", type->second);

    return 0;
}
