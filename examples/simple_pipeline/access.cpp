#include <fstream>
#include <fmt/core.h>
#include <cstdint>
#include <unistd.h>
#include "access.hh"
#include "transform.hh"
#include "broker.hh"
#include "protorpc/binaryreader.hh"

#include "simple_pipeline.sidl.hh"

namespace pipeline
{

Access::Access(rpc::ServiceId id, ipc::Port port)
    : ClientService(id, port)
{}

namespace
{

void line_cb_(rpc::Service& svc, rpc::Message& msg)
{
    fmt::print("[ACCESS] Block request received\n");
    Access& access = static_cast<Access&>(svc);
    constexpr std::size_t block_size = 64;

    std::vector<std::uint8_t> buff;
    buff.resize(block_size);

    ssize_t bytes_read = read(access.input_fd, buff.data(), block_size);

    if (bytes_read < 0)
        throw std::runtime_error("Error while reading from file");

    buff.resize(bytes_read);

    fmt::print("[ACCESS] Bytes read: {}\n", bytes_read);

    // Preparing the answer
    pipeline::messages::access::GetDataReply reply(msg.destination(), msg.source());

    // End of stream
    if (bytes_read == 0)
    {
        reply.set_block(std::nullopt);
    }
    else
    {
        pipeline::messages::Block block;
        block.set_move_data(std::move(buff));
        reply.set_block(block);
    }

    reply.set_token(msg.token());
    svc.send_message(reply);
}

}

void Access::setup_thread_()
{
    std::string file = "/etc/passwd";
    fmt::print("[ACCESS] Requesting input file '{}'\n", file);

    // Ask the broker nicely for a file
    messages::broker::OpenFileRequest file_req(id(), rpc::BrokerService);
    file_req.set_path(file);

    auto fut = send_request(file_req);
    messages::broker::OpenFileReply file_rep(std::move(fut.get()));
    auto error = file_rep.get_error();

    if (error)
        throw std::runtime_error(fmt::format("[ACCESS] Error while opening file: {}", error->get_error_message()));

    input_fd = file_rep.get_file_handle();

    // Now that we have the file descriptor we are ready to receive requests.
    bind(messages::access::GetDataRequestId, &line_cb_);

    // Create the transform process
    pipeline::messages::broker::CreateProcessRequest create_req(id(), rpc::BrokerService);
    create_req.set_process_name(std::string("transform"));

    fut = send_request(create_req);
    messages::broker::CreateProcessReply create_rep(std::move(fut.get()));
    auto create_err = create_rep.get_error();

    if (create_err)
        throw std::runtime_error(fmt::format("[ACCESS] Process creation failed: {}", create_err->get_error_message()));

    rpc::ServiceId transform_bid = create_rep.get_process_id();

    // Initialize the transform process
    messages::transform::InitializeRequest transform_init_req(id(), transform_bid);
    fut = send_request(transform_init_req);
    messages::transform::InitializeReply transform_init_rep(std::move(fut.get()));

    auto init_err = transform_init_rep.get_error();

    // TODO: Print the error message
    if (init_err)
        throw std::runtime_error("[ACCESS] Error while initializing transform");

    fmt::print("[ACCESS] Pipeline initialized, ready to receive requests\n");
}

void Access::init()
{
    std::thread setup_thread(&Access::setup_thread_, this);
    setup_thread.detach();
}

}
