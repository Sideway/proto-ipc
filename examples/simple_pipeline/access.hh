#ifndef ACCESS_HH
#define ACCESS_HH

#include "clientservice.hh"

namespace pipeline
{
    class Access: public ClientService
    {
    public:
        Access(rpc::ServiceId id, ipc::Port port);
        void init() override;

        int input_fd = -1;
    private:
        void setup_thread_();

    };
}

#endif
