#include <chrono>
#include "fmt/core.h"
#include "fmt/color.h"
#include "protorpc/binaryreader.hh"
#include "output.hh"

#include "simple_pipeline.sidl.hh"

namespace pipeline
{

    namespace {
        void read_all_data(Output& svc, rpc::ServiceId transform_id)
        {
            auto start = std::chrono::system_clock::now();
            std::uint64_t bytes_read = 0;

            for (;;)
            {
                messages::transform::GetDataRequest data_req(svc.id(), transform_id);
                auto fut = svc.send_request(data_req);
                messages::transform::GetDataReply data_rep(std::move(fut.get()));

                auto block_opt = data_rep.get_block();

                // Reached eof
                if (!block_opt)
                    break;

                auto block = *block_opt;
                auto block_data = block.get_data();
                bytes_read += block_data.size();

                std::string data(reinterpret_cast<char*>(block_data.data()), block_data.size());

                std::string data_str = fmt::format(fg(fmt::color::crimson), "{}", data);
                fmt::print("[OUTPUT] Received data: '{}'\n", data_str);

            }

            auto epoch = std::chrono::system_clock::now() - start;
            auto epoch_s = std::chrono::duration_cast<std::chrono::milliseconds>(epoch).count() / 1000.0f;

            fmt::print("[OUTPUT] Reached eof\n");

            if (bytes_read > 0)
                fmt::print("[OUTPUT] Read {} bytes in {}s ({}b/s)\n", bytes_read, epoch_s,
                        bytes_read / epoch_s);
        }

        void init_cb(rpc::Service& svc, rpc::Message& msg)
        {
            Output& output = static_cast<Output&>(svc);

            fmt::print("[OUTPUT] Initializing output\n");

            messages::output::InitializeReply init_rep(msg.destination(), msg.source());
            init_rep.set_token(msg.token());
            output.send_message(init_rep);

            // Now we read all data
            std::thread read_thread(&read_all_data, std::ref(output), msg.source());
            read_thread.join();
        }
    }

Output::Output(rpc::ServiceId id, ipc::Port port)
    : ClientService(id, port)
{
    bind(messages::output::InitializeRequestId, &init_cb);
}

}
