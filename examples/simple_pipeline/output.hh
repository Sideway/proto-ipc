#ifndef OUTPUT_HH
#define OUTPUT_HH

#include "clientservice.hh"

namespace pipeline
{
    class Output: public ClientService
    {
    public:
        static constexpr rpc::MessageType Initialize = 1;
        static constexpr rpc::MessageType SendLine = 2;

        Output(rpc::ServiceId id, ipc::Port port);
    };
}

#endif
