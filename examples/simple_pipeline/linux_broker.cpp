#include <cstdlib>
#include <fmt/core.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include "broker.hh"

namespace pipeline
{

void Broker::platform_setup_()
{
    epoll_fd_ = epoll_create1(0);

    if (epoll_fd_ == -1)
        throw std::runtime_error("epoll_create1 failed");
}

rpc::Message Broker::next_message()
{
    // TODO: epoll and find fd
    constexpr int EPOLL_MAX_EVENTS = 16;
    struct epoll_event events[EPOLL_MAX_EVENTS];

    int fscnt = epoll_wait(epoll_fd_, events, EPOLL_MAX_EVENTS, -1);

    if (fscnt == -1)
        throw std::runtime_error("epoll_wait failed");

    struct epoll_event ev = events[0];
    rpc::ServiceId bid = ev.data.u64;

    auto it = services_map_.find(bid);

    if (it == services_map_.end())
        throw std::runtime_error(fmt::format("[BROKER] Unknown source {} (reading)", bid));

    auto [sid, port] = *it;
    std::vector<std::uint8_t> message_data;
    std::vector<int> message_handles;

    // TODO: Retry on error ?
    port.receive(message_data, message_handles);
    rpc::Message message(std::move(message_data), std::move(message_handles));

    // Sanity checking
    if (sid != message.source())
        throw std::runtime_error(fmt::format("[BROKER] Source process {} tried to impersonate {}", bid, message.source()));

    return message;
}

rpc::ServiceId Broker::create_process(std::string name)
{
    if (name != "access" && name != "transform" && name != "output")
        throw std::runtime_error(fmt::format("Invalid process kind: {}", name));

    int pair[2];

    if (socketpair(AF_UNIX, SOCK_DGRAM, 0, pair) == -1)
        throw std::runtime_error("Could not create socket pair");

    // We mark the child socket as cloexec because we don't want forked processes
    // to inherit handles to the other processes.
    int child_fd = fcntl(pair[0], F_DUPFD_CLOEXEC, 0);

    if (child_fd == -1)
        throw std::runtime_error("Could not dup child fd\n");

    close(pair[0]);

    rpc::ServiceId child_id = current_bid_++;
    ipc::Port child_port(child_fd);

    services_map_.emplace(child_id, child_port);

    fmt::print("[BROKER] Adding child [id={}, fd={}]\n", child_id, child_port.handle());

    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.u64 = child_id;

    if (epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, child_port.handle(), &ev) == -1)
    {
        std::perror("epoll");
        throw std::runtime_error("epoll_ctl failed");
    }

    int pid = fork();

    // TODO: Cleanup on error
    if (pid == -1)
        throw std::runtime_error("Fork failed");

    if (pid == 0)
    {
        execl("/proc/self/exe",
                fmt::format("{}#{}", name, child_id).c_str(),
                "--kind",
                name.c_str(),
                "--port",
                fmt::format("{}", pair[1]).c_str(),
                "--bid",
                fmt::format("{}", child_id).c_str(),
                0);

        throw std::runtime_error("Execve failed");
    }

    close(pair[1]);
    fmt::print("[BROKER] Created '{}' process with bid: {}, pid: {}, fd: {}\n",
            name, child_id, pid, pair[0]);

    return child_id;
}

}
