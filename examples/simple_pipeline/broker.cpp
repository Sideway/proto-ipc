#include <optional>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <fcntl.h>
#include <fmt/core.h>
#include <fmt/color.h>
#include <unistd.h>
#include <sys/socket.h>
#include "broker.hh"

#include "protorpc/message.hh"
#include "protorpc/binaryreader.hh"
#include "simple_pipeline.sidl.hh"

namespace pipeline
{

void create_process_cb(rpc::Service& svc, rpc::Message& msg)
{
    Broker& b = static_cast<Broker&>(svc);

    // Parse the message
    messages::broker::CreateProcessRequest req(std::move(msg));
    std::string process_name = req.get_process_name();

    fmt::print("[BROKER] CreateProcess('{}') received from proccess bid {} (token = 0x{:x})\n",
            process_name, req.source(), req.token());

    rpc::ServiceId created_proc = b.create_process(process_name);

    // Send back the bid of the created process to the caller
    messages::broker::CreateProcessReply rep(req.destination(), req.source());
    rep.set_token(req.token());
    rep.set_process_id(created_proc);

    b.send_message(rep);

}

void report_error_cb(rpc::Service& svc, rpc::Message& msg)
{
    messages::broker::ReportError req(std::move(msg));

    fmt::print(fg(fmt::color::red), "[BROKER] Process {} crashed with error: {}\n",
            req.source(), req.get_error_message());

    std::exit(1);
}

void open_file_cb(rpc::Service& svc, rpc::Message& msg)
{
    messages::broker::OpenFileRequest open_req(std::move(msg));
    std::string path = open_req.get_path();

    fmt::print("[BROKER] Process {} requesting file '{}'\n", msg.source(), path);

    // Opening the file, on error fill the optional
    messages::broker::OpenFileReply open_rep(msg.destination(), msg.source());
    open_rep.set_token(open_req.token());

    int fd = open(path.c_str(), O_RDONLY);
    std::optional<messages::OpenFileError> error = std::nullopt;

    if (fd == -1)
    {
        messages::OpenFileError open_err;
        open_err.set_error_message(std::string(std::strerror(errno)));
        error = open_err;
    }
    else
    {
        open_rep.set_file_handle(fd);
    }

    open_rep.set_error(error);
    svc.send_message(open_rep);
}

Broker::Broker()
    : rpc::Service(0)
{
    platform_setup_();

    bind(messages::broker::CreateProcessRequestId, &create_process_cb);
    bind(messages::broker::ReportErrorId, &report_error_cb);
    bind(messages::broker::OpenFileRequestId, &open_file_cb);
    create_process("access");
}

void Broker::send_message(rpc::Message& message)
{
    auto dest = services_map_.find(message.destination());

    if (dest == services_map_.end())
        throw std::runtime_error(fmt::format("Trying to send message to invalid service (bid: {})", message.destination()));

    auto [id, port] = *dest;
    std::vector<std::uint8_t> message_data = message.serialize();
    std::vector<int> message_handles = message.handles();

    port.send(message_data, message_handles);
}

std::future<rpc::Message> Broker::send_request(rpc::Message& message)
{
    auto fut = register_reply(message);
    send_message(message);

    return fut;
}

void Broker::forward_message(rpc::Message& message)
{
    auto src = services_map_.find(message.source());

    if (src == services_map_.end())
        throw std::runtime_error("Source id does not exist");

    auto dst = services_map_.find(message.destination());

    if (dst == services_map_.end())
        throw std::runtime_error("Destination id does not exist");

    send_message(message);
}

}
