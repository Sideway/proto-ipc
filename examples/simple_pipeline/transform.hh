#ifndef TRANSFORM_HH
#define TRANSFORM_HH

#include "clientservice.hh"

namespace pipeline
{
    class Transform: public ClientService
    {
    public:
        static constexpr rpc::MessageType Initialize = 1;
        static constexpr rpc::MessageType SendLine = 2;

        Transform(rpc::ServiceId id, ipc::Port port);

        rpc::ServiceId access_bid = 0;
    };
}

#endif
