#include "clientservice.hh"

namespace pipeline
{

ClientService::ClientService(rpc::ServiceId id, ipc::Port port)
    : rpc::Service(id), broker_port_(port)
{}

rpc::Message ClientService::next_message()
{
    std::vector<std::uint8_t> message_data;
    std::vector<int> message_handles;

    // TODO: Error checking / retry ?
    broker_port_.receive(message_data, message_handles);

    return rpc::Message(std::move(message_data), std::move(message_handles));
}

void ClientService::send_message(rpc::Message& message)
{
    std::vector<std::uint8_t> message_data = message.serialize();
    std::vector<int> message_handles = message.handles();
    broker_port_.send(message_data, message_handles);
}

std::future<rpc::Message> ClientService::send_request(rpc::Message& message)
{
    auto fut = register_reply(message);
    send_message(message);

    return fut;
}

}
