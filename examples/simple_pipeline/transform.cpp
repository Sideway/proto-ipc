#include <algorithm>
#include <fmt/core.h>
#include <fmt/color.h>
#include "protorpc/binaryreader.hh"
#include "broker.hh"
#include "transform.hh"
#include "output.hh"

#include "simple_pipeline.sidl.hh"

namespace pipeline
{

namespace
{

void send_cb(rpc::Service& svc, rpc::Message& msg)
{
    Transform& transform = static_cast<Transform&>(svc);

    // We need to request from the access
    messages::access::GetDataRequest get_data_req(transform.id(), transform.access_bid);
    auto fut = transform.send_request(get_data_req);
    messages::access::GetDataReply get_data_rep(std::move(fut.get()));

    auto block_opt = get_data_rep.get_block();

    if (block_opt)
    {
        // We received data so we can modify it (reverse the byte array)
        auto block_data = block_opt->get_data();
        std::reverse(block_data.begin(), block_data.end());
        block_opt->set_move_data(std::move(block_data));
    }

    // Send back the modified data (or nothing) to output
    messages::transform::GetDataReply output_data_rep(msg.destination(), msg.source());
    output_data_rep.set_move_block(std::move(block_opt));
    output_data_rep.set_token(msg.token());

    transform.send_message(output_data_rep);
}

void init_cb(rpc::Service& svc, rpc::Message& msg)
{
    Transform& transform = static_cast<Transform&>(svc);
    transform.access_bid = msg.source();

    // We need to create the output process
    messages::broker::CreateProcessRequest create_req(transform.id(), rpc::BrokerService);
    create_req.set_process_name(std::string("output"));

    auto fut = transform.send_request(create_req);
    messages::broker::CreateProcessReply create_rep(std::move(fut.get()));
    auto create_err = create_rep.get_error();

    if (create_err)
        throw std::runtime_error(fmt::format("[TRANSFORM] Process creation failed: {}", create_err->get_error_message()));

    rpc::ServiceId output_bid = create_rep.get_process_id();

    // Initializing the output process
    transform.bind(messages::transform::GetDataRequestId, &send_cb);

    // Initializing the output
    messages::output::InitializeRequest init_req(transform.id(), output_bid);
    fut = transform.send_request(init_req);
    messages::output::InitializeReply init_rep(std::move(fut.get()));

    auto init_err = init_rep.get_error();

    if (init_err)
        throw std::runtime_error(fmt::format("[TRANSFORM] Error while initializing output: {}", init_err->get_error_message()));

    fmt::print("[TRANFORM] Output initialized\n");

    // Ack the initialization message
    messages::transform::InitializeReply transform_init_rep(msg.destination(), msg.source());
    transform_init_rep.set_token(msg.token());

    transform.send_message(transform_init_rep);
}

}

Transform::Transform(rpc::ServiceId id, ipc::Port port)
    : ClientService(id, port)
{
    bind(messages::transform::InitializeRequestId, &init_cb);
}

}
