#ifndef CLIENTSERVICE_HH
#define CLIENTSERVICE_HH

#include "protorpc/service.hh"
#include "protoipc/port.hh"

namespace pipeline
{
    class ClientService: public rpc::Service
    {
    public:
        ClientService(rpc::ServiceId id, ipc::Port port);
        virtual rpc::Message next_message() override;
        virtual void send_message(rpc::Message& message) override;
        virtual std::future<rpc::Message> send_request(rpc::Message& message) override;
    private:
        ipc::Port broker_port_;
    };
}

#endif
