#ifndef BROKER_HH
#define BROKER_HH

#include <vector>
#include <poll.h>
#include "protorpc/service.hh"
#include "protoipc/port.hh"

namespace pipeline
{
    class Broker : public rpc::Service
    {
    public:
        static constexpr rpc::MessageType CreateProcess = 1;
        static constexpr rpc::MessageType ReportError = 2;

        Broker();

        void send_message(rpc::Message& message) override;
        std::future<rpc::Message> send_request(rpc::Message& message) override;
        rpc::Message next_message() override;
        void forward_message(rpc::Message& msg) override;

        /*
         * Creates a new child process of kind 'name'
         */
        rpc::ServiceId create_process(std::string name);

    private:
        /*
         * Platform specific setup
         */
        void platform_setup_();

        /*
         * Routing table for the broker.
         */
        std::map<rpc::ServiceId, ipc::Port> services_map_;

        /*
         * Current broker id
         */
        rpc::ServiceId current_bid_ = 1;

#ifdef __linux__
        int epoll_fd_ = -1;
#else
        #error "Unsupported platform"
#endif
    };
}

#endif
