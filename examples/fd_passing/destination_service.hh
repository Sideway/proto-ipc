#ifndef DESTINATIONSERVICE_HH
#define DESTINATIONSERVICE_HH

#include "protorpc/service.hh"
#include "protoipc/port.hh"

namespace passing
{

    class Destination: public rpc::Service
    {
    public:
        Destination(ipc::Port broker_port);

        void send_message(rpc::Message& message) override;
        std::future<rpc::Message> send_request(rpc::Message& message) override;
        rpc::Message next_message() override;

    private:
        ipc::Port broker_port_;
    };

}

#endif
