#include <unistd.h>
#include <fcntl.h>
#include "source_service.hh"
#include "test_messages.sidl.hh"

namespace passing
{

Source::Source(ipc::Port broker_port)
    : rpc::Service(1), broker_port_(broker_port)
{}

void Source::send_message(rpc::Message& message)
{
    std::vector<std::uint8_t> message_data = message.serialize();
    std::vector<int> message_handles = message.handles();
    broker_port_.send(message_data, message_handles);
}

std::future<rpc::Message> Source::send_request(rpc::Message& message)
{
    auto fut = register_reply(message);
    send_message(message);

    return fut;
}

rpc::Message Source::next_message()
{
    std::vector<std::uint8_t> message_data;
    std::vector<int> message_handles;
    broker_port_.receive(message_data, message_handles);

    return rpc::Message(std::move(message_data), std::move(message_handles));
}

void Source::init()
{
    int fd_1 = open("/etc/passwd", O_RDONLY);
    int fd_2 = open("/etc/hosts", O_RDONLY);

    messages::destination::FdSend msg(1, 2);
    msg.set_first_fd(fd_1);
    msg.set_second_fd(fd_2);

    send_message(msg);
}

}
