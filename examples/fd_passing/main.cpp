#include <string>
#include <unistd.h>
#include <sys/socket.h>
#include "fmt/core.h"
#include "source_service.hh"
#include "destination_service.hh"

void launch()
{
    int pair[2];

    if (socketpair(AF_UNIX, SOCK_DGRAM, 0, pair) == -1)
        throw std::runtime_error("Could not create socketpair");

    int pid = fork();

    if (pid == 0)
    {
        fmt::print("[+] Starting Destination (pid: {})\n", getpid());
        close(pair[1]);
        passing::Destination(pair[0]).loop();
    }
    else
    {
        fmt::print("[+] Starting Source (pid: {})\n", getpid());
        close(pair[0]);
        passing::Source(pair[1]).loop();
    }
}

int main(int argc, char** argv)
{
    launch();
}
