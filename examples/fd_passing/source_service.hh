#ifndef SOURCESERVICE_HH
#define SOURCESERVICE_HH

#include "protorpc/service.hh"
#include "protoipc/port.hh"

namespace passing
{

    class Source: public rpc::Service
    {
    public:
        Source(ipc::Port broker_port);

        void send_message(rpc::Message& message) override;
        std::future<rpc::Message> send_request(rpc::Message& message) override;
        rpc::Message next_message() override;

        void init() override;

    private:
        ipc::Port broker_port_;
    };

}

#endif
