#include <unistd.h>
#include "fmt/core.h"
#include "destination_service.hh"
#include "test_messages.sidl.hh"

namespace passing
{

void fd_send_cb(rpc::Service& svc, rpc::Message& msg)
{
    passing::messages::destination::FdSend fdsend(std::move(msg));
    int handles[2] = {fdsend.get_first_fd(), fdsend.get_second_fd()};

    for (int handle : handles)
    {
        fmt::print("[DESTINATION] Reading 512 Bytes from handle {}\n", handle);
        char buffer[512] = {0};

        read(handle, buffer, sizeof(buffer) - 1);

        fmt::print("{}\n", buffer);
    }
}

Destination::Destination(ipc::Port broker_port)
    : rpc::Service(2), broker_port_(broker_port)
{
    bind(messages::destination::FdSendId, &fd_send_cb);
}

void Destination::send_message(rpc::Message& message)
{
    std::vector<std::uint8_t> message_data = message.serialize();
    std::vector<int> message_handles = message.handles();
    broker_port_.send(message_data, message_handles);
}

std::future<rpc::Message> Destination::send_request(rpc::Message& message)
{
    auto fut = register_reply(message);
    send_message(message);

    return fut;
}

rpc::Message Destination::next_message()
{
    std::vector<std::uint8_t> message_data;
    std::vector<int> message_handles;
    broker_port_.receive(message_data, message_handles);

    return rpc::Message(std::move(message_data), std::move(message_handles));
}

}
