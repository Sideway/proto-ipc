# proto-ipc

Proof of concept ipc library using a small idl (sidl).

## Library concept

The library handles the communication between processes as well as the main
event loop. Creating an ipc service only requires extending the ipc::Service
class and registering callbacks for specific messages.
Messages can be sent in two ways. The first one is unidirectional messages
which can be sent using the send_message method. The second is for messages
requiring an answer. The send_request method will send a message and return
a future on which the calling service can wait for an answer.

## Crafting messages

The prefered way of crafting messages is through sidl. The sidl compiler will
create all the boilerplate serialization code allowing the custom messages to
be converted to and from ipc::Message (which can be sent on the wire). You can
find the documentation for SIDL in the `sidl/` project directory.

## Examples

A few example projects are present inside `examples/`.

### simple_pipeline

Simple pipeline represents a simple data pipeline composed of three components.
The **Broker** is the main component. Its job is to create processes, handle
privileged requests (ex: open a file) and route messages between processes.
The **Access** service is responsible for bootstrapping the pipeline creation
process and is also the process providing the data to the whole pipeline.
The **Transform** service reads data from the access, modifies it (here it only
reverses the block of bytes read) and forwards it to the next component.
The last component is the **Output**. This service asks the transform for data
which it will then print onto the screen.

### fd_passing

This small example shows how messages can be used to pass file descriptors
between processes.
